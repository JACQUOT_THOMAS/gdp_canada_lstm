# Prediction du PIB canadien avec des méthodes de Deep Learning

Ce dépôt GitLab est dédié au projet final du cours de **DEEP LEARNING: MODELS AND OPTIMIZATION** de troisième année de l'ENSAE. 

Vous y trouverez notamment : 
- les données utilisées lors de la modélisation et téléchargées [ici](https://open.canada.ca/data/en/dataset/625c0d81-2eee-40cf-b402-17a26f3fb2e1)
- l'implémentation à travers un _notebook_ également visualisable sur [Google Collab](https://colab.research.google.com/drive/1zZnPX--hoCdF5DwqbSOH1Et-o4KD0BwV?usp=sharing)
- le rapport du projet 
